# README #

* is built using HTML + Javascript

### What is this repository for? ###

* This repo contains a HTML + Javascript Template
* HTML + JavaScript

### How do I get set up? ###

* git clone https://KudzanaiGomera@bitbucket.org/KudzanaiGomera/html-template-api.git
* cd html-template-api
* cd PetStore

### How to run? ###
* click index.html

### Contribution guidelines ###

* Writing tests -> Kudzanai Gomera
* Code review -> Junior and Sipho

### Who do I talk to? ###

* Kudzanai Gomera
* Katanga Dev Team